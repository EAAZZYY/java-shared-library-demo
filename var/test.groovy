#!/user/bin/env groovy

def call() {
    echo "starting test on $BRANCH_NAME branch"
    sh 'mvn test'
}