#!/user/bin/env groovy

def call() {
    echo "starting build-jar on $BRANCH_NAME branch"
    sh 'mvn package'
}